name: inverse
layout: true
class: inverse

---
template: inverse
background-image: url(img/DSC_0190.jpg)
background-size: 100%
background-repeat: no-repeat
background-position: center top
class: bottom middle

# Golden/Dark/Golden Age

## czyli czemu dziś jest tak zajebiście jak 20 lat temu


---

background-image: url(img/DSCF0024.JPG)
background-size: 100%

???

W roku 2000, wykładowca na Uniwersytecie prowadząc zajęcia z programowania, poinformował nas co następuje.

Po pierwsze primo, programowanie to ślepa uliczka bo i tak nie będziecie w stanie nadążyć za postępującym tempem rozwoju technologii, wypalicie się zawodowo, dostaniecie depresji, i w najlepszym wypadku będziecie uczyć przestarzałych rzeczy na uczelni, dokładnie tak samo jak on.

Po drugie primo, zawód programisty już niedługo przestanie istnieć, bo dzięki postępowi systemy będzie się rysowało w narzędziu graficznym, a kod wypluje nam sieć neuronowa na podstawie dwóch diagramów i pięciu zdań opisu.

---

background-image: url(img/PXL_20230324_191038105.jpg)
background-size: 100%

???

Piszę te słowa 20 lata później, siedząc na katamaranie na Karaibach, z kieliszkiem rum-punchu w dłoni, patrząc na roześmiane dziewczyny biegające w bikini po pokładzie i zastanawiając się, co kurwa poszło nie tak.

Dwa tygodnie żeglowania po Karaibach na katamaranie kosztuje 9k + lot. To znaczy miesiąc kosztuje w sumie około 24k. Senior dev Javy w Wawie, według No fluff jobs to 18-25k. Team leader, architekt to już stawki 38k, a gdy klient jest z USA czy Londynu, da się wyciągnąć 60k.

Chyba nie jest tak źle z tym programowaniem.

Ale nie zawsze tak było

---


layout: false
background-image: url(img/102_0206_r1.jpg)
background-size: 70%
background-position: center top

???

Zacząłem karierę 20 lat temu od nekromancji. Odziedziczyłem w spadku mały projekt i zadanie: sprawić by zmartwychwstał również w tym roku. Z delikatnymi zmianami oczywiście. Wszystko w nim smierdziało trupem, od zgniłej wieki temu technologii, aż po martwicę mózgu autora widoczną w każdej linijce (if else {} bo nie znał zaprzeczenia). Po miesiącu archeologii zgłosiłem zadanie wykonane, za co zebrałem tęgie baty, bo jak to, projekt był rozpisany na rok to ma zająć rok, w jakim świetle ty nas stawiasz, jeszcze wyjdzie że inni się opierdalają. Zderzenie z korpo-ścianą było potężne. Pograłem jeszcze miesiąc w half life z chłopakami w pracy i w końcu stwierdziłem że życie jest zbyt ciekawe by spędzić je w nekropolii.

---

layout: false
background-image: url(img/marszkuklesce.jpeg)
background-size: 100%
background-position: center middle

???

W 2003 pracowałem w projekcie "Tytan"

Pierwszą poważną pracę dostałęm w systemie który nazwano ostatecznie Tytan. Na cześć bezsensownego wysiłku, który złamał życiu co najmniej kilkunastu osobom, kosztował miliony i zabrał 3 lata życia. Został niedoszacowany jedynie o 600%, co zresztą było wiadomo od samego początku, ale management był odporny na rzeczywistość bardziej niż zwolennicy pisu. Podstawowymi elementami architektonicznymi były moduły wielkości dzisiejszych Bounded Contextów oraz diagram związków encji (ERD, baza danych) rozklejony na wszystkich ścianach każdego pokoju). Czytaliśmy wtedy takie motywacyjne książki jak Mityczny Osobomiesiąc, Projektowanie czyli marsz ku klęsce. W zespole sporo osób zastanawiało się nad migracją do Australii lub zostaniem leśniczym w Kanadzie.

---

layout: false
background-image: url(img/mythical+man+month.jpg)
background-size: 100%
background-position: center middle


???

1973, książka 1975

---

layout: false
background-image: url(img/Ken_Thompson_and_Dennis_Ritchie--1973.jpg)
background-size: 60%
background-position: center middle


???

Make each program do one thing well. [...] build afresh rather than complicate old programs by adding new "features"

Expect the output of every program to become the input to another, as yet unknown, program

Design and build software [...] to be tried early, ideally within weeks

Use tools in preference to unskilled help [...]
---

layout: false
background-image: url(img/dsc_5505.jpg)
background-size: 100%
background-position: center middle

???

Dostałem robotę w małym software housie w Warszawie. Robota wyglądała tak: szedłem na pierwsze spotkanie do nowego klienta. Pokrótce opowiadał mi czego potrzebuje. Ze spotkania  przynosiłem wymagania. Pół dnia potrzebowałem na rozpisanie ich w scenariusze behawioralne, zaprojektowanie aplikacji, narysowanie modelu danych. Kolejne pół na mockupy zużywał grafik a ja robiłem wycenę. Jak klient klepnął po 1-3 miesiącach mieliśmy gotowy system na produkcji. Sami go hostowaliśmy i monitorowaliśmy. Buffor w budżecie na popełnianie błędów w estymacji wynosił 2 dni.

Firma zatrudniła Javowców do SMSów. Jak zobaczyli co robimy to musieli długo szczęki zbierać z podłogi. Mieli tylko jedno pytanie, czemu w PHP a nie Java + Spring. Przecież mogliśmy wziąć open source'a zamiast pisać cały framework i ORM'a. Na to mój szef odpowiedział: bo phpowcy taniej kosztują. Sprawdziłem, faktycznie w Londynie w tamtym czasie PHPowiec zarabiał 35k funtów, a Javowiec 50k. Rzuciłem robotę i przeszedłem na C#, potem na Javę.

---

layout: false
background-image: url(img/DSC_0028.jpg)
background-size: 100%
background-position: center middle


???

6 lat pracowałem w małym softwarehousie który nie zatrudniał ani jednego człowieka od sprzedaży. Zaproszenia do przetargów dostawaliśmy dzięki temu że nasi klienci polecali nas kolejnym. Budowaliśmy systemy dla telekomów, banków, ale również startupów. Jedną rzecz miały wspólną - zawsze były budowane maksymalnie jednym zespołem (11 devów). 

Zastanawiało mnie jak to jest że wygrywamy przetargi z HP, Microsoftem i innymi sławnymi firmami. Aż w końcu zrozumiałem, gdy klient do nas zadzwonił z prośbą, byśmy podnieśli wycenę, bo jesteśmy o rząd wielkości tańsi od konkurencji, i trochę to dziwnie wygląda w przetargu.

Jakim cudem byliśmy tańsi o rząd wielkości. Poszedłem pogadać z konkurencją i się wyjaśniło. Otóż duże firmy (HP, Microsoft, IBM, etc.) prowadziły głównie duże projekty (na kilkadziesiąt, kilkaset devów), coś czego my nie robiliśmy. W takich przypadkach dodawali olbrzymi narzut na zarządzanie, korpo-beton, politykę, i cały ten syf który pamiętałem z wielkich projektów. I wychodziło im 10x więcej kosztów.

---

layout: false
background-image: url(img/83725055_3019861601411407_773746166624944128_n.png)
background-size: 80%
background-position: center middle

???

Meta-meta model

Najlepszy przykład korpo-narzutu miałem gdy poszedłem na rozmowę do jednej z takich firm. Trafiłem na szefa działu architektury, uber-architekta, który miał pod sobą kilkudziesięciu architektów.
Jego pierwsze pytanie na rozmowie brzmiało: jak rysuję meta-meta modele.
Dla osób które nie wiedzą o co cho - model w architekturze może dotyczyć danych, relacji agentów/aplikacji, lub domeny. Meta model to model tego w jaki sposób się modeluje. Czyli po krótce z czego się składa i jakie są zależności w procesie modelowania. Meta-meta model, to model tego, jak się modeluje modelowanie modeli. Czujecie temat?
Firma miała chujową wydajność, feedback loop kilkumiesięczny, tylko testerów manualnych i generalnie była ciężko w dupie pod względem poziomu developerki, a koleś mnie pyta o meta-meta modele.

Tak w tamtych czasach wyglądało robienie projektów na kilkaset osób - droga przez mękę.

W małych software housach pracowało się zajebiście. Ale miały jedną wadę - nigdy nie dostawaliśmy projektów na kilkadziesią czy 100 osób. Z perspektywy architekta to problem, bo projekty na kilkadziesiąt, kilkaset czy kilka tysięcy osób są ciekawsze architektonicznie.

No ale albo robimy projekty na jeden zespół, albo walczymy z organizacją, polityką i mitycznym osobomiesiącem.


---

layout: false
background-image: url("img/16.05.2012 - 1.jpg")
background-size: 100%
background-position: center middle

???

I wtedy w 2012 roku, na 33rd Degree (protoplasta  Devoxx PL), Thoughtworks ogłosił że rozwiązali problem Freda Brooksa, czyli jak wydajnie robić software na tysiące osób. 
Wystarczy stworzyć niezależne, autonomiczne zespoły, które traktują swoje "domeny" jako produkty, wyjebać całą zbędną otoczkę organizacyjną, modele kanoniczne, meta modele, szyny danych, i połączyć to wszystko prostym protokołem asynchronicznym (u nich Atom) zgodnie z pomysłem (smart-endpoints & dumb pipes).

Mikroserwisy albo śmierć (nie zatrudnisz developerów jak ich nie masz).

---

layout: false
background-image: url("img/DSC_0015.JPG")
background-size: 100%
background-position: center middle

???

Na początku tego roku zrobiłem w Łodzi szkolenie, gdzie na sali było 12 developerów, średnia doświadczenia 5 lat zawodowego, i wszyscy robili tylko i wyłącznie mikroserwisy.

I mnie olśniło.

Ci ludzie nigdy nie będą musieli
- bać się wdrożenia
- brać nadgodziny żeby zdążyć przed releasem
- tłumaczyć się czemu projekt przekroczył budżet o 600%
- zastanawiać się czemu mają nowego kierownika 3 raz w tym roku
- wkurwiać się dlaczego mają przestarzałą wersję języka o 6 lat
- wkurwiać się czemu nie mogą użyć biblioteki której chcą użyć
- a muszą używać jakiegoś syfu którego nikt nie rozumie i nikt nie lubi
- ściemniać klientowi prosto w oczy
- wkurwiać się na innego człowieka który rozjebał im projekt a którego na oczy nigdy nie widzieli


Co więcej, nie wystąpi u nich impostor syndrom, bo nic tak nie pomaga na wątpliwośći czy ja umiem programować, jak 4k rpsów na produkcji z error rate'm 0% pokazujące że jednak umiesz.

---

class: center middle


Future

It's going to get better


???

Martin Thompson 2015: Intel Optane

---


layout: false
background-image: url("img/optane.png")
background-size: 70%
background-position: center middle


???

Przewidywanie przyszłości jest trudne

---

layout: false
background-image: url("img/337869007_198931196170073_927583881527467240_n.jpg")
background-size: 60%
background-position: center middle

---

layout: false
background-image: url("img/arnold.jpg")
background-size: 100%
background-position: center middle

???

Po pierwsze, to jest martwy zawód już od 23 lat.

Po drugie, macie teraz idealnego partnera do pair programmingu.

A w przyszłości 

---

layout: false
background-image: url("img/eclipsePhase.webp")
background-size: 100%
background-position: center middle

---

class: center middle


# Dzięki

jakubn@gmail.com 

https://nabrdalik.dev/

linkedin: linkedin.com/in/jnabrdalik/

twitter: jnabrdalik

mastodon: @jnabrdalik@mstdn.social

This presentation is available at <br />
https://jakubn.gitlab.io/goldendarkage/




